# Exercices

## Apache ET Nginx

Chaque question est donc à traiter deux fois.

1. Activez les journaux : mettez en place un journal d’erreur et un journal de requêtes.
1. Modifiez le journal de requêtes pour qu’il indique l’URL demandée ainsi que le numéro de processus fils la traitant.
1. A l’aide d’un de vos camarades, tracez les requêtes venant d’une adresse IP particulière dans un fichier séparé (utilisé en cas d’analyses fines d’attaques) : la règle à utiliser pour Apache s’intitule `Remote_Addr`, pour Nginx, vous utiliserez une `map`.
1. Remplissez vos journaux via l’utilisation d’un utilitaire de benchmarking comme `siege`, et analysez leur contenu à l’aide d’outils tel Webalizer^[ <http://www.mrunix.net/webalizer>], AwStats^[ <http://awstats.sourceforge.net/>] ou tout autre outil du même type de votre choix (cherchez et proposez un comparatif des méthodes de quelques-uns de ces outils).

## Apache uniquement

1. Regardez le fonctionnement du module `mod_usertrack`.

