# Cadre général des serveurs Web

## Historique

- Création de l’HTML en 1989 par Tim \textsc{Berners-Lee} au \textsc{Cern} à Genève.
- En 1992, 26 serveurs Web étaient recensés dans le monde par le \textsc{Cern}, chacun d’eux étant une implémentation raisonnablement stable des protocoles existants.
- En 1993, le \textsc{Cern} rend disponible son propre serveur Web tandis qu’aux États-Unis, le \textsc{Ncsa} met un navigateur, appelé Mosaic, à disposition. Ces deux événements vont permettre la démocratisation du Web.
- En 1994, le serveur Web le plus populaire était `httpd`, produit par le \textsc{Ncsa}, mais laissé en friche après le départ de son développeur principal, Rob \textsc{McCool}.
- En 1995, de nombreux programmeurs se regroupe dans un consortium appelé *Apache group* et créent Apache à partir de `httpd`. Apache signifie d’ailleurs *A patchy server*.
- L’année suivante, Apache était déjà le serveur Web le plus utilisé.
- Après l’abandon des serveurs Web développés par le \textsc{Cern} et le \textsc{Ncsa}, Apache ne quitte plus sa place de leader (bien qu’il soit sur une pente descendante) et fait fonctionner *a priori* ±30% des sites Web actifs actuels^[ source : <https://news.netcraft.com/archives/2018/10/29/october-2018-web-server-survey.html>], suivi par Nginx qui fait tourner un peu plus de 21% des sites actifs actuels.

## Définitions

### Principe du Web

Le Web est fondé sur l’utilisation de 3 composantes principales :


- les URI (*Uniform Resource Identifier*), permettant d’identifier une ressource accessible via Internet ;
- le langage HTML (*HyperText Markup Language*), langage de description de données ;
- le protocole HTTP (*HyperText Transfer Protocol*), permettant de formaliser et de normaliser des échanges entre les clients et les serveurs Web.

### Rôles d’un serveur Web

**Rôle principal** : application attendant les requêtes des clients sur un port précis d’une machine donnée, afin de leur transmettre des documents.
Les échanges entre client et serveur sont normalisés via le protocole HTTP.

**Rôles secondaires** : donner des méta-informations sur les documents transmis, entretenir des communications avec des applications tierces (base de données, réseau) généralement dans le but de transmettre des documents qui sont générés de façon *dynamique*, c’est-à-dire à la demande.

## Le protocole HTTP

## Les différentes normes

Le protocole a peu évolué depuis sa création : seulement 4 normes se sont succédées.

- La première version, appelée 0.9, était très simple. Cette norme se contentait de fournir les documents qui étaient demandés.
- La norme 0.9 a été remplacée en mai 1996 par la norme 1.0, clairement définie par la RFC 1945^[ <https://tools.ietf.org/html/rfc1945>].
L’ajout le plus important a été l’utilisation d’en-têtes décrivant le type des données transmises, indiquant ainsi aux clients la façon de traiter les données qu’ils reçoivent.
- La norme 1.1, date de juin 1999 et est décrite par la RFC 2616^[ <https://tools.ietf.org/html/rfc2616>] bien que sa première définition date de 1997, dans la RFC 2068^[ <https://tools.ietf.org/html/rfc2068>].
Les apports de cette norme sont des méthodes supplémentaires de communication entre les clients et le serveur, ainsi que le support des hôtes virtuels.
- Une nouvelle norme, HTTP/2 a été déposée en mai 2015 dans la RFC 7540^[ <https://tools.ietf.org/html/7540>].
Apache et Nginx permettent son utilisation, respectivement depuis les versions 2.4.17 et 1.9.5^[ consulter <https://en.wikipedia.org/wiki/HTTP/2> pour plus de détails].
Elle est cependant plus une surcouche à HTTP/1.1 qu'une évolution de celle-ci, contrairement aux précédentes évolutions.

## Les méthodes de requête

Toutes les requêtes du protocole HTTP commencent par un en-tête indiquant la *méthode* de requête.

Ces méthodes, au nombre de 9, sont répertoriées dans le tableau suivant :

+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| **Méthode** | **Action**                                                                                                                                  |
+=============+=============================================================================================================================================+
| `GET`       | Permet de récupérer le document correspondant à l’URL joint à la requête                                                                    |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `HEAD`      | Identique à la requête `GET`, mais le serveur ne renvoie alors que l’en-tête de la réponse                                                  |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `POST`      | Permet de récupérer le document correspondant à l’URL joint à la requête, tout en envoyant des données qui sont requises par cette URL      |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `OPTIONS`   | Demande les options de communication du serveur, permettant ainsi une négociation pour déterminer la communication qui sera la plus adaptée |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `TRACE`     | Demande à ce que le message de requête revienne au client, ce qui lui permet de voir exactement ce que reçoit le serveur                    |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `PUT`       | Permet d’envoyer un document au serveur, qui sera enregistré dans l’URL passée en paramètre, ou modifié s’il existe déjà                    |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `PATCH`     | Permet d’envoyer des modifications partielles à un document                                                                                 |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `DELETE`    | Demande au serveur de supprimer la ressource identifiée dans l’URL de la requête                                                            |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+
| `CONNECT`   | Demande à un mandataire Web, typiquement un proxy, de tunneliser une connexion du client vers le serveur, plutôt que de simplement relayer la demande. Peut être typiquement utilisé pour demander à un proxy d’établir une connexion SSL vers un serveur |
+-------------+---------------------------------------------------------------------------------------------------------------------------------------------+

## Les en-têtes des clients

Une fois la méthode de requête précisée, les clients peuvent également envoyer des données au serveur, soit en utilisant l’*en-tête* HTTP si ce sont des informations complémentaires sur la requête, soit en utilisant le corps du message si ce sont des documents.

Les informations complémentaires sont envoyées grâce à une composition des sections présentées dans le tableau suivant (une section par ligne, liste non exhaustive) :

+-------------------+-----------------------------------------------------------------------------------------+
| **Section**       | **Description**                                                                         |
+===================+=========================================================================================+
| `Accept`          | Précise les types MIME acceptés par le client                                           |
+-------------------+-----------------------------------------------------------------------------------------+
| `Accept-Encoding` | Précise les encodages de caractères acceptés par le client                              |
+-------------------+-----------------------------------------------------------------------------------------+
| `Accept-Language` | Précise les langues acceptées par le client                                             |
+-------------------+-----------------------------------------------------------------------------------------+
| `Host`            | Nom de l’hôte (et éventuellement numéro de port) concerné par la requête                |
+-------------------+-----------------------------------------------------------------------------------------+
| `User-Agent`      | Informe le serveur sur le client (généralement le navigateur) à l’origine de la requête |
+-------------------+-----------------------------------------------------------------------------------------+

**NB** : on peut tout à fait envoyer des en-têtes forgés ne correspondant à rien comme `X-Sisterhood-of-Karn` (on les préfixe généralement avec `X`, mais cela n’a rien d’obligatoire).
Normalement, ces en-têtes ne seront pas traités par le serveur ou le web, mais on peut tout à fait décider de les utiliser (dans les logs, pour une authentification, etc).

## Les résultats renvoyés par le serveur Web

Si la requête se conforme à HTTP/0.9, le serveur se contente de renvoyer le document qui a été demandé.

À partir de la version 1.0, il renvoie également d’autres informations avant de renvoyer finalement le document demandé (si celui-ci peut être transmis bien sûr).

La première ligne de la réponse indique la version du protocole utilisé par le serveur Web, ainsi que la valeur de retour.
Cette valeur est en 2 parties : un code et un commentaire, correspondant au libellé du code.
Ces codes sont rangés par catégories, comme le montre le tableau suivant :

+-----------+-----------------------------------------------+
| **Codes** | **Catégorie de la réponse**                   |
+===========+===============================================+
| 100-199   | Information                                   |
+-----------+-----------------------------------------------+
| 200-299   | Succès de la requête du client                |
+-----------+-----------------------------------------------+
| 300-399   | Redirection de la requête du client           |
+-----------+-----------------------------------------------+
| 400-499   | Requête du client incomplète ou non autorisée |
+-----------+-----------------------------------------------+
| 500-599   | Erreurs du serveur                            |
+-----------+-----------------------------------------------+

Le code 200 représente en particulier un succès de la requête.
Les tranches de code décrites comportent environ 60 codes différents^[ pour plus de détails, voir <https://en.wikipedia.org/wiki/List_of_HTTP_status_codes>].

La suite des en-têtes renvoyés par le serveur est une composition des sections suivantes, suivant le type de demande et la configuration du serveur (liste non exhaustive) :

+--------------------+-----------------------------------------------------------------------------------------+
| **Section**        | **Description**                                                                         |
+====================+=========================================================================================+
| `Connection`       | Précise les options de cette connexion réseau                                           |
+--------------------+-----------------------------------------------------------------------------------------+
| `Content-Encoding` | Indique le schéma d’encodage (compression généralement) associée au contenu             |
+--------------------+-----------------------------------------------------------------------------------------+
| `Content-Length`   | Taille du corps du message                                                              |
+--------------------+-----------------------------------------------------------------------------------------+
| `Content-Type`     | Décrit le type MIME du contenu                                                          |
+--------------------+-----------------------------------------------------------------------------------------+
| `Date`             | Date d’émission de la réponse                                                           |
+--------------------+-----------------------------------------------------------------------------------------+
| `Expires`          | Précise une date et l’heure après lesquelles le document fourni sera obsolète           |
+--------------------+-----------------------------------------------------------------------------------------+
| `Last-Modified`    | Précise la date et l’heure de la dernière modification du document fourni               |
+--------------------+-----------------------------------------------------------------------------------------+
| `Location`         | Est associé à une URL vers laquelle le client est redirigé                              |
+--------------------+-----------------------------------------------------------------------------------------+
| `Server`           | Informations sur le serveur ayant renvoyé la requête                                    |
+--------------------+-----------------------------------------------------------------------------------------+

Finalement, le document demandé est renvoyé, si cela est possible bien sûr.

## Les types MIME

Les types MIME permettent de préciser le type de documents transmis lors d’une communication.

MIME, signifiant *Multipurpose Internet Mail Extensions*, a tout d’abord été conçu pour décrire le type des pièces jointes à des mails.

Ces types ont par la suite été repris dans HTTP/1.0 pour que les clients aient connaissance du type de document qui leur est renvoyé.

Les types MIME sont décrits par les RFC 1521^[ <https://tools.ietf.org/html/rfc1521>] et 1522^[ <https://tools.ietf.org/html/rfc1522>].

Quelques exemples de types MIME, catégorie par catégorie :

- texte : `text/plain`, `text/html`, `text/xml`
- image : `image/gif`, `image/jpg`, `image/png`
- audio : `audio/aiff`, `audio/mp3`, `audio/basic`
- vidéo : `video/mpeg`, `video/x-ms-asf`, `video/mpeg`
- application : `application/pdf`, `application/zip`, `application/x-latex`

# Présentation générale d’Apache

## Introduction

## Apache et la concurrence

Apache est actuellement leader des serveurs Web avec un déploiement proche du tiers de ce type de serveurs.
Des solutions alternatives existent cependant :

- libres : nginx, lighttpd, thttpd, Mathopd, Boa…
- commerciales : Stronghold (Red Hat), Oracle iPlanet Web Server (Oracle), IIS (Microsoft)…

Certains de ces serveurs présentent des avantages sur Apache, dans des contextes particuliers d’utilisation ou simplement parce qu’ils sont associés à une assistance technique.

## Principales caractéristiques d’Apache

- Conformité aux standards : Apache est complètement conforme à la norme 1.1.
- Hébergement virtuel : Apache est capable de gérer plusieurs sites Web (relatifs à plusieurs domaines distincts) sur une même machine.
- Objets dynamiques partagés : Apache peut charger des modules d’extension de ses fonctionnalités en cours d’exécution et ces modules peuvent être intégrés sous Apache sans avoir à le recompiler.
- Personnalisation et extensibilité : Apache peut être étendu par des modules programmé en C ou en Perl utilisant l’API Apache, ce qui permet d’étendre ses fonctionnalités pour un besoin particulier.
- Sécurité : par la prise en compte de protocoles de sécurité tels que SSL/TLS.
- La communauté du libre et en particulier celle de l’*Apache Software Foundation* !

## Installation et exécution d’Apache

Plusieurs solutions existent pour installer Apache :

- l’installer à partir de ses sources, ce qui permet d’obtenir une version optimisée pour une architecture et un besoin particulier ;
- l’installer à partir d’un package, dépendant de la distribution utilisée (ou de la plate-forme).

Le démarrage d’Apache correspond à l’exécution du programme `apache2`^[ selon la distribution ou la méthode d’installation, cela peut être le programme `httpd`], soit en ligne de commande, soit via un script de démarrage^[ je dirais plus souvent « script de démarrage » que « service systemd », mais ça veut dire la même chose pour moi] ce qui est le cas le plus fréquent.

Quelques options du programme `apache2` (parmi les nombreuses disponibles) sont très utiles :

- `-v` : obtenir la version d’Apache
- `-V` : identique à l’option `-v`, en ajoutant d’autres informations comme les options de compilation utilisées ou les répertoires par défaut…
- `-l` : liste des modules compilés de façon statique
- `-M` : liste des modules chargés par Apache
- `-t` : vérifie la syntaxe des fichiers de configuration sans lancer le serveur
- `-f` : utilise le fichier de configuration passé en paramètre plutôt que le fichier de configuration par défaut.

Le fichier principal de configuration d’Apache s’appelle `httpd.conf` ou `apache2.conf` sur une Debian.
Il sera souvent modifié dans la suite de ce cours.
Nous garderons le nom d’`apache2.conf` tout au long de ce cours.

# Présentation générale de Nginx

## Principales caractéristiques de Nginx

- Contrairement à Apache qui utilise par défaut une approche par *thread* ou par *process* répondant chacun à une requête, Nginx utilise une approche par évènement^[ également disponible pour Apache avec le module `mpm_event`].
Imaginons qu’on vous demande de faire du café et tout de suite après d’aller chercher le courrier : vous pouvez préparer la cafetière, la mettre en marche, attendre devant que le café soit prêt et ensuite aller chercher le courrier, ou alors vous pouvez, une fois la cafetière allumée, aller chercher le courrier et revenir chercher la cafetière une fois que c’est prêt.
La deuxième approche est celle de Nginx, ce qui lui permet supporter un grand nombre de connections simultanées avec une empreinte mémoire réduite.
- Contrairement à Apache qui permet d’ajouter des modules dynamiquement (il suffit de les activer), les différents modules de Nginx doivent être inclus lors de sa compilation.
Ce qui explique pourquoi on retrouve plusieurs paquets fournissant Nginx dans Debian (`nginx`, `nginx-light`, `nginx-full` et `nginx-extras`).
Enfin, ça c’était vrai [avant la version 1.9.11](http://nginx.org/en/linux_packages.html#dynmodules).
La version de Nginx de Debian Stretch étant la 1.10.3, nous bénéficierons donc de ces modules dynamiques, mais il est fort probable que vous rencontriez d’anciennes versions où les modules seront compilés statiquement.
- Support des scripts FastCGI^[ <https://en.wikipedia.org/wiki/FastCGI>], SCGI^[ <https://en.wikipedia.org/wiki/Simple_Common_Gateway_Interface>], WSGI^[ <https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface>].
- Nginx peut servir de répartiteur de charge (*load balancer*).
- Son système de cache est très appréciable.
- Nginx est aussi un excellent proxy inverse ainsi qu’un proxy mail.

## Installation et exécution de Nginx

Plusieurs solutions existent pour installer Nginx :

- l’installer à partir de ses sources, ce qui permet d’obtenir une version optimisée pour une architecture et un besoin particulier (par exemple pour n’y inclure que les modules nécessaires) ;
- l’installer à partir d’un package, dépendant de la distribution utilisée (ou de la plate-forme).
Dans ce cas, prenez soin de vérifier que les modules dont vous avez besoin sont bien fournis.

Le démarrage de Nginx correspond à l’exécution du programme `nginx`, soit en ligne de commande, soit via un script de démarrage ce qui est le cas le plus fréquent.

Quelques options du programme `nginx` sont très utiles :

- `-v` : obtenir la version de Nginx.
- `-V` : identique à l’option `-v`, en ajoutant d’autres informations comme les options de compilation utilisées.
Vous pourrez y voir la liste des modules inclus dans votre version de Nginx.
- `-t` :vérifie la syntaxe des fichiers de configuration
- `-s SIGNAL` : permet d’envoyer un signal au serveur nginx lancé. `SIGNAL` peut être :
    - `stop` : ferme brutalement (sans attendre d’avoir servi les requêtes en cours)
    - `quit` : ferme une fois toutes les requêtes traitées
    - `reload` : recharge la configuration
    - `reopen` : rouvre les journaux systèmes
