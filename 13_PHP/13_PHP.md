PHP^[ <https://secure.php.net/>] est un langage extrêmement répandu.
Voici comment servir des fichiers PHP avec Apache et Nginx.

Ceci est le strict minimum, certaines applications nécessitent des configurations plus complexes.

# Fichier d’exemple

Créez le fichier `/var/www/html/index.php` contenant le code suivant :

```php
<?php php_info() ?>
```

# Apache

Installez le module PHP (il s’activera tout seul à l’installation) :

```
apt install libapache2-mod-php
```

Modifiez la directive `DirectoryIndex` de votre hôte virtuel :

```
DirectoryIndex index.php index.html
```

Allez sur votre hôte virtuel : vous devriez avoir une page d’information sur la version de PHP installée sur votre machine et sur ses modules.

# Nginx

Nginx ne peut pas interpréter directement les fichiers PHP comme le fait Apache.

On utilisera `PHP-FPM`^[ <https://fr.wikipedia.org/wiki/PHP-FPM>] via le protocole FastCGI.

Installez-le :

```
apt install php-fpm
```

Modifiez la configuration de votre hôte virtuel :

```
index index.php index.html;
location ~ \.php$ {
   include snippets/fastcgi-php.conf;

   # With php-fpm (or other unix sockets):
   fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
   # With php-cgi (or other tcp sockets):
   #fastcgi_pass 127.0.0.1:9000;
}
```

On notera que Nginx peut communiquer avec `PHP-FPM` via par une *socket unix* ou une *socket* réseau.

Rechargez Nginx :

```
nginx -t && nginx -s reload
```

Allez sur votre hôte virtuel : vous devriez avoir une page d’information sur la version de PHP installée sur votre machine et sur ses modules.
