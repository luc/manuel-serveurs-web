# Exercices

Les exercices sont à rendre sur <https://lstu.fr/asrall-web-tp4>.
Envoyez vos réponses dans un fichier appelé `tp4_votre_nom.txt`.

Vous pouvez envoyer un seul fichier pour un binôme, mais pensez bien à mettre vos deux noms dans le nom du fichier.

1. **Apache** : Quelles sont les fonctionnalités offertes par le module `mod_dir`^[ <https://httpd.apache.org/docs/2.4/mod/mod_dir.html>] ?
   Quelle manipulation simple permet de mettre en évidence son rôle dans les URL qui ne se terminent pas par des `/` ?
1. **Apache et Nginx** : Testez les alias et les réécritures sur quelques exemples.
1. Mettez en place des réécritures de votre choix.
  Cependant :
    - **Apache** : pour vos tests mettez en place des logs spécifiques pour les réécritures,
    - essayez vos règles dans des `.htaccess` (**Apache**) et dans des vhosts spécifiques (**Apache et Nginx**),
    - **Apache et Nginx** : mettez en place des réécritures conditionnelles.
  Par exemple en réécrivant l’URL si la requête provient d’une certaine IP.
1.  À l’aide de l’outil `ab`, essayez de mettre en avant l’incidence que peut avoir une règle de réécriture sur les performances d’Apache et Nginx.

