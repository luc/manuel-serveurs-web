**Un petit mot sur les hôtes virtuels**

C’est bien expliqué sur Wikipédia^[ <https://fr.wikipedia.org/wiki/H%C3%A9bergement_virtuel>, consulté le 12 nov. 2018], alors boum, citation :

> En informatique, l'hébergement virtuel (de l'anglais virtual hosting abrégé vhost) est une méthode que les serveurs tels que serveurs Web utilisent pour accueillir plus d'un nom de domaine sur le même ordinateur, parfois sur la même adresse IP, tout en maintenant une gestion séparée de chacun de ces noms. Cela permet de partager les ressources du serveur, comme la mémoire et le processeur, sans nécessiter que tous les services fournis utilisent le même nom d'hôte. Le terme hébergement virtuel (virtual hosting) est utilisé habituellement en référence aux serveurs Web, mais les principes s'appliquent également à d'autres services internet.

Dans notre cas, les hôtes virtuels sont les différents sites web servis par Apache ou Nginx.

# Apache

La documentation officielle, <https://httpd.apache.org/docs/> deviendra vite votre meilleure amie.

## Introduction

Le comportement d’Apache est contrôlé par un ensemble d’instructions appelées *directives*.
Ces directives se décomposent en deux groupes : les directives *natives*, disponibles sur toute installation d’Apache, et les directives d’*extension*, liées aux extensions installées sur un serveur Apache particulier.

Sous certaines distributions, le fichier de configuration principal, `apache2.conf` est organisé de façon à sous-traiter les directives qu’il est sensé contenir à d’autres fichiers, contenus parfois dans des sous-répertoires du répertoire principal d’installation.
Ce mécanisme est utilisé afin de faciliter la maintenance d’Apache par rapport à la philosophie de ces distributions.
Sous Debian, par exemple, il faut considérer le fichier `apache2.conf` pour la configuration principale d’Apache, les sous-répertoires `conf-xxx` pour des morceaux de configurations supplémentaires, les sous-répertoires `mod-xxx` pour les modules d’extension et enfin les sous-répertoires `sites-xxx` pour la configuration des hôtes virtuels (les sites web).

Pour plus de lisibilité, le fichier `apache2.conf` est généralement divisé en 3 sections :

1. *Configuration de l’environnement global*, traitant essentiellement du fonctionnement global des processus rattachés à Apache ;
2. *Section principale*, contenant les directives du serveur principal. Ces directives sont également utilisées par défaut pour les hôtes virtuels si ceux-ci n’ont pas donné de nouvelles directives ;
3. *Section des hôtes virtuels*, utilisée lorsqu’Apache traite les requêtes concernant plusieurs sites Web sur une même machine.

L’inclusion de sous-répertoires permet de séparer le configuration en petits blocs facilement repérables, comme `conf-available/security.conf`.

## Portée et contexte des directives

Chaque directive agit dans un contexte particulier.
Il est important de connaître et de comprendre ces contextes pour mesurer la *portée* des directives qui y sont définies.
Les quatre contextes existants sous Apache sont :

- *Le contexte général* du serveur.
Ce contexte contient des directives qui ne peuvent être présentes qu’à cet endroit, ainsi que d’autres directives pouvant être également définies dans d’autres contextes.
Pour ces dernières directives, une définition dans le contexte général du serveur permet de fixer une valeur par défaut à ces directives dans le cas où elles ne sont pas redéfinies dans d’autres contextes.
- *Le contexte conteneur*. Ce contexte inclut les directives inclues dans un des 3 conteneurs suivant : `<Directory>` (relatif au répertoire utilisé pour répondre à la requêt), `<Files>` (relatif au fichier supposé être renvoyé), `<Location>` (relatif à l’URI demandée).
- *Le contexte d’hôte virtuel*, défini par le conteneur `<VirtualHost>`.
- *Le contexte `.htaccess`*, qui sont des fichiers de configuration complémentaires pouvant être placés dans les répertoires gérés par Apache.
Le but de ces fichiers est de décentraliser les directives liées à Apache, en permettant par exemple à un utilisateur « lambda » de configurer les requêtes liées aux données qui le concerne sans avoir à modifier le fichier `apache2.conf` (ce qu’il ne peut pas faire normalement de toute façon).

## Configuration du contexte général du serveur

- `Servername` : contient le nom d’hôte de la machine sur laquelle s’exécute le serveur.
- `ServerRoot` : correspond au répertoire d’installation d’Apache, contenant en particulier ses fichiers de configuration.
Il est possible de changer au démarrage d’Apache la valeur donnée par cette directive grâce à l’option `-d`.
C’est à partir de ce répertoire que seront calculés les chemins relatifs contenus dans les autres directives (comme `Include` par exemple).
- `Include` : charge la configuration contenue dans les fichiers spécifiés
- `DocumentRoot` : permet de définir la racine des documents distribués par Apache.
Ce chemin est ainsi utilisé comme base pour le calcul des noms de fichiers correspondant aux requêtes reçues par le serveur.
- `ScriptAlias` : permet de spécifier un répertoire contenant des scripts exécutables (se termine généralement par `cgi-bin`).
- `ErrorDocument` : permet de changer le comportement par défaut d’Apache lorsqu’une erreur est rencontrée.
Cette directive permet une redéfinition par code d’erreur, en affichant un message différent ou en redirigeant la requête vers un document traitant l’erreur en question.
- `DefaultType` : permet de redéfinir le type MIME par défaut des documents renvoyés par le serveur (rarement utilisée dans un contexte général).
- `User` et `Group` : permettent de définir sous quelle identité s’exécuteront les fils du processus principal Apache.
- `DirectoryIndex` : définit dans l’ordre la liste des fichiers qui doivent être recherchés lorsqu’une requête concerne un répertoire.
Le premier fichier présent est renvoyé.
- `UserDir` : permet de fournir des répertoires Web individuels aux utilisateurs d’un système.
- `Options` : permet de contrôler certaines fonctionnalités du serveur dans un répertoire particulier.
Cette directive peut être associée aux valeurs suivantes (un « + » en préfixe d’une valeur active cette option — action par défaut — et un « - » la désactive) :
    - `ExecCGI` : autorise l’exécution de script CGI
    - `FollowSymLinks` : le serveur peut suivre les liens symboliques de ce répertoire, mais cela ne change pas la correspondance établie avec les sections `<Directory>`.
    - `Includes` : autorise les SSI (*Server-Side Includes*)
    - `IncludesNOEXEC` : autorise les SSI, mais leurs commandes `#exec` et `#include` sont désactivées
    - `Indexes` : renvoie une liste formatée de ce répertoire si une requête concernant ce répertoire est établie et qu’il n’existe pas de fichier correspondant à une des valeurs de la directive `DirectoryIndex`.
    - `Multiviews` : autorise les multiviews à contenu négocié (par exemple pour fournir `foo.fr.html` ou `foo.en.html` selon la préférence du navigateur quand la ressource `foo` est demandée)
    - `SymLinksIfOwnerMatch` : suit les liens symboliques dont le fichier ou le répertoire appartient au même utilisateur que le lien.
    - `All` : inclut toutes les options, sauf `Multiviews`. C’est la valeur par défaut.
    - `None` : aucune de ces options.
- `IndexOptions` (avec la valeur `FancyIndexing`) : fournit un listage du contenu des répertoires plus agréable lorsque l’option `Indexes` est activée.

# Nginx

La documentation officielle, <http://nginx.org/en/docs/> vous sera, ici aussi, très précieuse.
Tout particulièrement les pages <http://nginx.org/en/docs/dirindex.html> et <http://nginx.org/en/docs/varindex.html>.
Créez-vous des marque-pages dans votre navigateur !

Comme pour Apache, on retrouvera un découpage de la configuration en plusieurs fichiers répartis dans différents répertoires.

Sur Debian :
- `nginx.conf` : fichier de configuration générale, qui contient des inclusions d’autres fichiers de configuration ;
- `modules-xxx` : dossiers contenant les fichiers permettant l’activation des modules de Nginx ;
- `conf.d` : contient des fichiers de configuration générale pour le serveur web (Nginx peut aussi faire office de proxy mail) ;
- `sites-xxx` : configuration des hôtes virtuels.

Le dossier `snippets` est particulier : il ne fait pas l’objet d’inclusions dans la configuration de base.
Il est là pour vous permettre de ranger des bouts de configuration que vous pourrez alors vous-même inclure dans vos configuration.
L’intérêt est de mutualiser la configuration (à quoi bon écrire 20 fois la même configuration alors qu’une inclusion est plus rapide ?).

## Directives et contextes

- une directive est constituée d’un nom de directive, de paramètres séparés par des espaces et se terminant par `;` :

    ```
    ssl on;
    ```

- les contextes ont une structure similaire à ceci près qu’ils se terminent par des accolades encadrant des directives ou des contextes supplémentaires :

    ```
    http {
        server {
            server_name example.org;
        }
    }
    ```

## Portée des directives

Les directives peuvent être positionnées dans zéro, un ou plusieurs contextes.
Lorsqu’elles ne peuvent être positionnées dans aucun contexte, on parlera du contexte `main`, correspondant plus ou moins^[ voir la section suivante] au contexte général d’Apache vu plus haut.

Les contextes possibles pour une directive peuvent varier selon cette directive^[ ceci est aussi valable pour Apache]. Ainsi, si `internal` ne peut être placé que dans le contexte `location`, `ignore_invalid_headers` peut être placé dans les contextes `http` et `server`.

Pour trouver dans quel contexte doit se situer une directive, reportez-vous à la documentation officielle.
La liste alphabétique des directives et des contextes est disponible sur <http://nginx.org/en/docs/dirindex.html>.

# Contextes Apache et Nginx

## Différences

**!!! ATTENTION !!!** Ne pas confondre les contextes Apache et Nginx, j’utilise le même terme mais ils ne désignent pas la même chose.

Les contextes Nginx sont plutôt similaires aux conteneurs d’Apache.

Si le contexte `main` ressemble au contexte général d’Apache, il existe toutefois une différence de taille.
En effet, Nginx n’est pas qu’un serveur web, il peut aussi faire office de proxy mail.
Dès lors, les directives du contexte général d’Apache relatives au programme lui-même (`User` et `Group`) auront bien généralement des directives équivalentes (`user`, qui permet aussi de définir le groupe auquel appartient le processus), tandis que celles qui ne sont utiles que dans le cadre d’un serveur web, comme `ErrorDocument` auront une directive équivalente à placer dans le contexte `http`.
Et d’autres, bien qu’on puisse penser qu’elles pourraient se placer dans le contexte `http` puisqu’étant dans le contexte général d’Apache, ne pourront se placer que dans `server` (par exemple).

Le contexte conteneur d’Apache n’a pas vraiment d’équivalent, tant sont diverses les combinaisons entre directives et contextes dans lesquels on peut les placer.

Le contexte d’hôte virtuel d’Apache est, lui, comparable au contexte `server`.

Enfin, le contexte `.htaccess` n’existe tout simplement pas avec Nginx.
Ceci possède des avantages et des inconvénients.

- On ne peut laisser la possibilité à un utilisateur « lambda » de modifier le comportement du serveur dans les répertoires le concernant : on risque donc plus d’avoir des demandes d’utilisateur.
De plus, de nombreux projets proposent des fichiers `.htaccess` pour adapter le serveur aux besoins particuliers du projet : il faudra traduire ces besoins en configuration Nginx (ce sont généralement des réécritures d’URL, un vrai bonheur à traduire).
- Par contre, comme Nginx ne regardant pas dans chaque répertoire traversé s’il n’existe pas un `.htaccess` à prendre en compte, la vitesse est accrue.
De plus, cela évite les erreurs de configuration des utilisateurs, sur lesquelles on s’arrache parfois les cheveux à cause d’un `.htaccess` mal configuré.

## Équivalents Nginx des directives de configuration du contexte général d’Apache

Je vous présente mes excuses pour cet inventaire à la \textsc{Prévert}, mais j’essayerai, lors de ce cours, et autant que faire ce peut, de vous permettre de configurer aussi bien Apache que Nginx pour des mêmes tâches.

Certaines directives pourront prendre place dans le contexte `main`, d’autres dans `http` et d’autres encore dans d’autres contextes^[ quand je vous disais que les contextes Apache et Nginx étaient différents].

- `Servername` : `server_name` (contexte : `server`).
- `ServerRoot` : pas de directive équivalente. Correspond à l’option `--prefix` utilisée lors de la compilation.
- `Include` : `include` (contextes : tous)
- `DocumentRoot` : `root` (contextes : `http`, `server`, `location`, `if` in `location`).
- `ScriptAlias` : pas d’équivalent, Nginx ne permettant pas d’utiliser des scripts CGI.
- `ErrorDocument` : `error_page` (contextes : `http`, `server`, `location`, `if` in `location`), mais ne permet pas de spécifier de message. Uniquement une page ou une redirection.
- `DefaultType` : `default_type` (contextes : `http`, `server`, `location`).
- `User` et `Group` : `user` (contexte :`main`).
- `DirectoryIndex` : `index` (contextes : `http`, `server`, `location`).
- `UserDir` : pas d’équivalent, mais on peut se débrouiller avec des expressions rationnelles dans un contexte `location`^[ il suffit de chercher des exemples sur le net, comme <https://gist.github.com/alanbriolat/1248004>].
- `Options` : pas d’équivalent, mais on trouve parfois des directives équivalentes pour les options.
    - `ExecCGI` : pas d’équivalent.
    - `FollowSymLinks` : `disable_symlinks` (contextes : `http`, `server`, `location`).
    - `Includes` : `ssi` (contextes : `http`, `server`, `location`, `if` in `location`).
    - `IncludesNOEXEC` : pas d’équivalent.
    - `Indexes` : `autoindex` (contextes : `http`, `server`, `location`).
    - `Multiviews` : pas d’équivalent.
    - `SymLinksIfOwnerMatch` : voir `disable_symlinks`.
    - `All` et `None` : pas d’équivalent.
- `IndexOptions` : pas d’équivalent.

