# Exercices

Si vous avez des questions à me poser, envoyez-les à asrall[CHEZ]fiat-tux.fr.
Merci de bien vouloir :

- utiliser votre adresse étudiante fournie par l’université (cela m’évite d’avoir à chercher qui m’a envoyé le mail si vous oubliez de mettre votre nom. `darklordofchaos78@bidule.com` n’est pas très parlant pour moi)
- mettre comme sujet `[asrall][TP 1]`
- mettre votre nom dans le mail

Les exercices sont à rendre sur <https://lstu.fr/asrall-web-tp1>.
Envoyez vos réponses dans un fichier appelé `tp1_votre_nom.txt`.

Vous pouvez envoyer un seul fichier pour un binôme, mais pensez bien à mettre vos deux noms dans le nom du fichier.

Ces exercices ont pour but, non pas de récupérer forcément de bonnes réponses (vous êtes là pour apprendre après tout), mais de voir comment vous appréhendez les problèmes et comment vous vous y prenez pour vous en sortir.
Les exercices ont aussi pour but de vous habituer à rechercher des informations dans la documentation officielle des projets.
En effet, à force d’habitude, vous saurez où chercher l’information idoine rapidement.

La réponse à ces exercices est **OBLIGATOIRE**.
Des points bonus pourront être attribués si les exercices ont bien été faits et que les réponses ne sont pas dénuées de sens.

## Apache

**!!! ATTENTION !!!** Ne modifiez **JAMAIS** la configuration du *virtualhost* par défaut pour les exercices !

Faites toujours :

```
cd /etc/apache2/sites-available
cp 000-default.conf td1.conf
a2dissite 000-default.conf
a2ensite td1.conf
apachectl -t
apachectl graceful
```

Cela vous permettra d’avoir toujours une configuration propre sous la main, et de ne pas effacer vos travaux de la semaine précédente.

`a2dissite` et `a2ensite` ne sont pas des outils fournis par Apache mais par Debian. Ils permettent de désactiver et d’activer aisément des sites dans la configuration d’Apache.
Il existe aussi `a2dismod` et `a2enmod` pour désactiver et activer rapidement des modules Apache, ainsi que `a2disconf` et `a2enconf` pour désactiver et activer les morceaux de configuration contenus dans les sous-répertoires `conf-xxx`.

`apachectl -t` effectue une vérification de la configuration.
Pensez à toujours utiliser cette commande avant de relancer Apache.
Si une configuration ratée ne prête pas à conséquence en cours, il en va tout autrement en production.

Prenez les bonnes habitudes tout de suite, relancez donc Apache avec :

```
apachectl -t && apachectl graceful
```

-------------------------

Installez Apache sur votre machine et lancez-le.
Pointez un navigateur sur l’URL <http://ip_de_la_machine> pour vérifier que tout marche bien.

### Configuration par défaut

1. Décrivez l’organisation des fichiers de configuration proposée par votre distribution.
2. Donner un moyen (simple) de lire **toute** la configuration utilisée par le serveur sur la sortie standard^[ Vous cherchez donc à concaténer des fichiers] (et donc de la lire confortablement avec votre `$PAGER` favori) ;

### Quelques exercices de configuration

En vous basant sur le cours, en vous inspirant des fichiers de configuration et en parcourant la documentation relative à Apache, répondez aux questions suivantes :

1. Comment définir le fichier renvoyé par défaut lorsque la requête correspond à un nom de répertoire plutôt qu’à un nom de fichier ?
Faites un test en définissant `index.html` pour un répertoire et `accueil.html` pour un autre.
Placez ces deux fichiers dans chacun des répertoires et vérifiez votre configuration.

Vous devez avoir une arborescence similaire à :
```
└── var
    └── www
        └── html
            ├── foo
            │   ├── index.html
            │   └── accueil.html
            └── bar
                ├── index.html
                └── accueil.html
```

2. Comment autoriser le listage du contenu d’un répertoire ?
Faites un test sur un répertoire pour vérifier votre configuration.
Quel est l’impact pour les sous-répertoires du répertoire d’origine ?
Comment supprimer le listage dans les sous-répertoires ?
3. Afficher le contenu des répertoires avec un style « fancy », ou sans ce style si votre serveur était déjà configuré avec ce style.
4. Comment autoriser les utilisateurs de votre système à utiliser un répertoire de leur *home directory* via Apache ?
Vérifiez que votre solution marche.
5. Configurez votre serveur Web pour qu’il :
    - supporte les connexions persistantes ;
    - accepte 50 requêtes au maximum par connexion ;
    - crée 4 processus au démarrage ;
    - accepte au maximum 30 clients en même temps.

Comment avez-vous vérifié la prise en compte de ces valeurs ?

## Nginx

**!!! ATTENTION !!!** Ne modifiez **JAMAIS** la configuration du *server* par défaut pour les exercices !

Faites toujours :

```
cd /etc/nginx/sites-available
cp default td1
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/td1 /etc/nginx/sites-enabled/
nginx -t
nginx -s reload
```

Cela vous permettra d’avoir toujours une configuration propre sous la main, et de ne pas effacer vos travaux de la semaine précédente.

`nginx -t` effectue une vérification de la configuration.
Pensez à toujours utiliser cette commande avant de relancer Nginx.
Si une configuration ratée ne prête pas à conséquence en cours, il en va tout autrement en production.

Prenez les bonnes habitudes tout de suite, relancez donc Nginx avec :

```
nginx -t && nginx -s reload
```

-------------------------

Installez Nginx sur votre machine et lancez-le.
Pointez un navigateur sur l’URL <http://ip_de_la_machine> pour vérifier que tout marche bien.

Si Nginx ne veut pas se lancer, demandez-vous si par hasard vous n’avez pas installé Apache sur la même machine et s’il ne squatte pas déjà le port 80 : un même port ne peut être utilisé que par un processus à la fois.

### Quelques exercices de configuration

1. Comment définir le fichier renvoyé par défaut lorsque la requête correspond à un nom de répertoire plutôt qu’à un nom de fichier ?
Faites un test en définissant `index.html` pour un répertoire et `accueil.html` pour un autre.
Placez ces deux fichiers dans chacun des répertoires et vérifiez votre configuration.
2. Comment supprimer le listage du contenu d’un répertoire ?
Faites un test sur un répertoire pour vérifier votre configuration.
Quel est l’impact pour les sous-répertoires du répertoire d’origine ?
Comment autoriser le listage dans les sous-répertoires ?

## Connexion avec `telnet`

Effectuez cet exercice avec Apache ou Nginx, cela n’a pas d’importance.

`telnet` permet d’établir des connexions à distance, en précisant éventuellement le port demandé.
Sachant que les serveurs Web s’exécutent normalement en écoutant les requêtes du port 80, il est alors possible de se connecter avec `telnet` sur un serveur Web et de lui faire des requêtes en utilisant le protocole HTTP.

1. Connectez-vous avec la commande `telnet localhost 80` et établissez une requête avec la commande `GET /`.
Quel résultat vous est retourné ?
Comment se finit la connexion ?
2. La commande précédente correspondait à la norme 0.9.
Pour établir une connexion en utilisant 1.0, tapez maintenant la commande `GET / HTTP/1.0`.
Que constatez-vous par rapport à la commande précédente ?
3. Enfin, pour établir une commande utilisant 1.1, utilisez la commande `GET / HTTP/1.1`.
Que constatez-vous ?
4. Pour remédier au problème de la question précédente, il faut ajouter le nom du serveur auquel vous faites la demande (`localhost` dans notre cas).
Retapez la commande en ajoutant `Host: localhost` sur la ligne suivante.
Quel est, selon vous, l’intérêt de cette évolution du protocole ?

