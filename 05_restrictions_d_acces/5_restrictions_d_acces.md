# Restrictions en fonction de l’origine d’un client

## Apache

Certaines directives et conteneurs permettent de définir des restrictions d’accès à tous les fichiers d’un répertoire, *via* les conteneurs `<Directory>` ou *via* le fichier `.htaccess` (ce type de restriction ne peut pas être associé directement à un fichier donné).
Ces directives sont fournies par des module `mod_auth*_*`, dont un sous-ensemble est généralement activé par défaut par les distributions.

Dans Apache 2.2 (que vous rencontrerez sûrement un jour ou l'autre), les restrictions sont mises en place au moyen des directives `Order`, `Allow` et `Deny`.
Ces directives sont obsolètes avec Apache 2.4, elles sont donc à éviter (bien qu'on puisse les utiliser grâce au module `mod_access_compat`).

La directive permettant ces restrictions `Require`.
Les modules `mod_authz_core` et `mod_authz_host` mettent à disposition des fournisseurs d'autorisation génériques utilisables avec la directive Require.

On les utilise ainsi :

```
Require *option* *fournisseur d'autorisation* *arguments*
```

Parmi les fournisseurs d'authentification, citons :

- `all`, qui prend en argument `granted` ou `denied` pour autoriser ou bloquer toutes les requêtes ;
- `ip`, qui prend une ou plusieurs IP ou réseaux au format CIDR^[ <https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation>] ;
- `host`, qui prend en argument tout ou partie d'un nom d'hôte qui sera comparé au nom d'hôte du client via une double interrogation DNS (à éviter) ;
- `local`, qui ne prend pas d'argument, et qui autorisera l'accès si le client est la machine locale (`127.0.0.0/8`, `::1` ou si l'IP du client et du serveur sont les mêmes) ;
- `method`, qui prend en argument une ou plusieurs méthodes HTTP.

Pour bloquer une requête, on utilisera l'option `not`^[ sauf pour `all`, puisqu'on a l'argument `denied`] :

```
Require not local
```

Pour grouper différentes directives d'autorisation, on pourra utiliser les conteneurs `<RequireAll>` (toutes les directives doivent correspondre pour autoriser l'accès), `<RequireAny>` (au moins une directive doit correspondre) ou `<RequireNone>` (aucune directive ne doit correspondre).

Si les directives ne sont pas dans un conteneur `<Require*>`, on considérera qu'elles sont dans un conteneur `<RequireAny>`.

Un peu de documentation sur <https://httpd.apache.org/docs/2.4/fr/howto/access.html>.

## Nginx

C'est le module `ngx_http_access_module`^[http://nginx.org/en/docs/http/ngx_http_access_module.html>] qui fournit les directives `allow` et `deny`, auquelles on donnera un paramètre (une adresse IP, un réseau au format CIDR ou `all`).

```
deny 192.0.2.1;
allow 192.0.2.0/24;
allow 198.51.100.0/24;
deny all
```

Les directives sont évaluées de haut en bas : la première qui correspond s'applique.

