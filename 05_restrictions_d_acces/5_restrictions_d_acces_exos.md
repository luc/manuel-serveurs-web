# Exercices

## Apache

1.  Examinez les modules `mod_expires` et `mod_info`.  Mettez en œuvre le dernier sur votre serveur.

## Nginx

1. Utilisez le module `ngx_http_stub_status_module` pour rendre disponibles des informations basiques sur votre serveur Nginx depuis le chemin `/status`, avec accès limité à votre `localhost` ainsi qu'à votre voisin.

