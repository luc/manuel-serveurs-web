
Les méthodes relatives à la sécurité vues précédemment ne permettent pas de s’affranchir de tous les problèmes de sécurité.
En effet, si des réponses partielles ont été apportées sur les problèmes d’authentification par exemple, les données échangées entre le serveur et les clients transitent en clair sur le réseau, ce qui n’est pas souhaitable lorsque ces informations sont trop sensibles (c’est typiquement le cas pour le commerce électronique par exemple).
D’autre part, les clients n’ont jamais la garantie qu’ils s’adressent au serveur auquel ils pensent s’adresser.
Il y a donc un problème d’authentification, non plus des utilisateurs, mais des serveurs eux-mêmes.

Pour répondre à ces deux problèmes, le protocole SSL/TLS^[ <http://fr.wikipedia.org/wiki/Transport_Layer_Security> N’ergotons pas, je vais parler de SSL tout du long, mais j’entends par là SSL/TLS. SSL est désormais à proscrire, il ne faut plus utiliser que TLS, mais les habitudes (et les noms des directives) ont la vie dure.], reposant sur des certificats d’authenticité, est supporté par Apache, Nginx et la plupart des serveurs web.
Sous Apache 2, ce protocole est géré par le module `mod_ssl`^[ <https://httpd.apache.org/docs/2.4/mod/mod_ssl.html>] et `ngx_http_ssl_module`^[ <http://nginx.org/en/docs/http/ngx_http_ssl_module.html>] pour Nginx.

**ATTENTION** On ne dit pas « crypter », « encrypter », « désencrypter » ou « cryptage ». On dit « chiffrer », « déchiffrer » et « chiffrement ». Le « chiffrage », c’est évaluer le coût de quelque chose. Par contre, le « décryptage » existe : c’est retrouver le message de départ d’un message chiffré sans posséder la clé de déchiffrement. Allez lire <https://chiffrer.info/> pour plus de détails.

# Quelques notions de cryptologie

Au niveau cryptologique, on distingue deux grandes familles de méthodes de chiffrement : les symétriques et les asymétriques.
Les méthodes symétriques reposent sur l’utilisation d’une seule clé, utilisée aussi bien pour chiffrer que pour déchiffrer une information.
La sécurité est alors basée sur la non-divulgation de la clé (qui doit rester secrète) et sur la difficulté de la deviner.
Ces méthodes sont très rapides à l’exécution mais posent des problèmes de confidentialité, étant donné que l’expéditeur et le destinataire doivent tous les deux connaître une même clé, qui doit forcément être communiquée avant d’initier les communications cryptées.
Ce type de technique est très difficile à mettre en œuvre à large échelle, typiquement pour des sites de commerce électronique.

À la différence de ces premières méthodes, les méthodes asymétriques reposent sur l’utilisation de deux clés, une publique et une privée, liées par une relation mathématique.
L’une d’entre elles est utilisée pour le chiffrement (la clé publique), tandis que l’autre est utilisée pour le déchiffrement (la clé privée).
Ces clés sont également utilisées pour faire de l’authentification.
Les méthodes à base de clés asymétriques sont nettement plus lentes, et donc plus coûteuses en temps de traitement, que les méthodes à base de clés symétriques.
Elles procurent cependant un niveau de sécurité très élevé.

SSL (*Secure Sockets Layer*) et TLS (*Transport Layer Security*) reposent sur des algorithmes de chiffrement aussi bien symétriques qu’asymétriques, selon les principes énoncés ci-dessus.
Les clés utilisées sont transmises au moyen de certificats, émis par des organismes appelés *autorités de certification*.
Les navigateurs Web intègrent par défaut une liste de telles autorités^[ sous Firefox, cette liste est consultable dans les préférences, rubrique « Avancé », bouton « Gérer les certificats », onglet « Autorités »].
Les certificats utilisés sont au format X.509.
Lorsqu’un site Web désire proposer des connexions sécurisées au moyen de SSL, une demande de certification doit être formulée auprès d’une autorité de certification.
Ces autorités peuvent être celles intégrées aux navigateurs^[ elles représentent alors des *tiers de confiance*], ce qui est l’usage pour les sites s’adressant au public le plus large, ou être confondues directement avec le site proposant le chiffrement, ce qui est le cas lorsque les utilisateurs visés sont restreints.
Dans ce dernier cas, ce sont alors le plus souvent des certificats autosignés^[ CAcert (<http://cacert.org>) est une autorité de confiance non présente dans les navigateurs. Elle n’est pas considéré comme un tiers de confiance, mais les certificats signés par cette autorité ne sont pas des certificats auto-signés.].

Les certificats autosignés, ainsi que les certificats signés par des autorités atypiques comme CAcert, sont sans aucun doute voué à quasiment disparaître depuis la création de Let’s encrypt^[ <https://letsencrypt.org/>].  
Let’s encrypt est une initiative de différents acteurs du Net pour fournir une autorité de certification gratuite et promouvoir l’utilisation du chiffrement.  
Gratuit, simple à mettre en place et très simplement automatisable, Let’s encrypt a réussi, en trois ans, à générer plus de 146 millions de certificats dont plus de 87 millions actifs à ce jour^[ <https://letsencrypt.org/stats/>].

Pour résumer, il convient donc de bien différencier les *clés*, utilisées pour chiffrer les communications entre deux acteurs, et les *certificats*, dont le but est d’établir l’authenticité d’un acteur (un serveur Web).

# Préparation du certificat

Le logiciel `openssl` est généralement associé à la bibliothèque `libssl` ; il permet en particulier de créer des clés à partir de plusieurs méthodes de chiffrement.
Une des méthodes les plus populaires est la méthode RSA, autrefois protégée par un brevet qui a expiré en septembre 2000.

1. Créer une clé de serveur.  
   Cette clé doit être unique pour chaque site Web à protéger et constituera une partie du certificat qui, une fois signé par une autorité de certification, permettra l’authentification du site auprès des clients (typiquement les navigateurs Web).
   Le nom de la clé est arbitraire, mais est généralement construit à partir du nom du site associé à la clé, et possède généralement l’extension `.key`.

    Lors de la création de la clé, une phrase clé peut être demandée.
    Cette phrase fonctionne comme un mot de passe, mais peut être plus longue, ce qui renforce la sécurité qui lui est associée.
    Il faut donc, comme pour les mots de passe, la retenir pour pouvoir utiliser par la suite la clé qui sera générée (lors de la création du certificat en particulier).

    On se passe généralement de mot de passe (mettre un mot de passe vide) pour un certificat serveur. Le mot de passe n’est généralement utilisé que pour les certificats *signants*, c’est à dire les certificats utilisés pour signer votre certificat.

    En effet, en cas de redémarrage du serveur, il faudra redonner la phrase clé.
    Quand le serveur crashe à 3h du matin, il attendra que vous soyez levé pour pouvoir démarrer le serveur Web.
    Un exemple typique d’utilisation d’`openssl` pour générer une telle clé est présentée ci-dessous.

         openssl genrsa [-des3] -out example.org.key 4096

    L’option `des3`, si elle est présente, chiffre la clé, ce qui nécessitera un mot de passe

1. Créer une demande de signature numérique de certificat X.509.  
   La phrase clé demandée au cours de cette étape est la même que celle fournie lors de la création de la clé de serveur, si vous en aviez une.

         openssl req -new -key example.org.key -out example.org.csr

1. Pour créer une clé et une demande de signature d’un seul coup, on peut utiliser :

         openssl req -new -newkey rsa:4096 -sha512 -nodes -out example.org.csr \
            -keyout example.org.key -subj "/C=FR/ST=/L=Example/O=Example/CN=example.org"

1. Obtenir le certificat.  
   À partir du fichier `.csr` obtenu, il est alors possible de s’adresser à une autorité de certification pour faire établir le certificat correspondant.
   Dans l’optique d’un site Web destiné à être accessible par un grand nombre d’utilisateurs, il est alors judicieux de choisir une autorité reconnue par le plus grand nombre possible de navigateurs.
   Cette opération se fait auprès de sociétés commerciales (par exemple Verisign, Thawte…), est payante et est limitée dans le temps (les certificats délivrés ont une date d’expiration).

    Pour d’autres types d’utilisations, si l’intérêt principal d’un certificat réside plus dans le chiffrement des données que dans l’authentification du site hébergeur, il est également possible de gérer sa propre autorité de certification et ainsi de produire des certificats *autosignés*.
    Les certificats produits devront alors être acceptés explicitement par tous les navigateurs utilisés.
    Cette solution est généralement adoptée pour les réseaux d’entreprise, ne souhaitant pas assurer des communications sécurisées pour le grand public, mais pour un nombre restreint d’utilisateurs, typiquement les employés de cette entreprise.
    Cette solution est décrite dans le paragraphe suivant.

    Dans tous les cas, le certificat établi correspond *in fine* à un fichier à extension `.crt` ou `.pem`.

Pour Let’s encrypt, la procédure est bien plus simple.
On installe un client Let’s encrypt, par exemple le client officiel `certbot`^[ <https://certbot.eff.org/>], et on le lance avec les options idoines, genre :

    certbot certonly --rsa-key-size 4096 --webroot -w /var/www/html/ -d example.org

Et boum ! Pour peu que l’on ait bien choisi ses options et que le serveur web soit bien configuré, on a un certificat qui nous attend dans `/etc/letsencrypt/live/example.org`.

Bien évidemment, les options et les chemins peuvent changer selon le client Let’s encrypt choisi.

Pour créer un certificat Let’s encrypt, il faut être en mesure de prouver que l’on maîtrise bien le nom de domaine du certificat demandé, que ce soit par un *challenge* HTTP (le client dépose un fichier sur votre serveur web et l’autorité va le récupérer) ou DNS (vous créez un enregistrement DNS que l’autorité va consulter).

# Apache

L’installation et la configuration d’Apache se réalise de la manière suivante :

1. Vérifier que `mod_ssl` est bien installé (`ls /etc/apache2/mods-available/ssl.*`)^[ s’il ne l’est pas… faut-il que je vous fasse un dessin ? :p].
1. Configurer Apache pour l’utilisation de SSL pour le certificat obtenu.
   Il faut tout d’abord activer le module SSL sous Apache.
   Suivant les installations d’Apache, cette activation peut être réalisée :

    - en relançant Apache avec le support de SSL *via* la commande : `httpd -DSSL` ou `apachectl startssl` (commande disparaissant avec Apache 2.4) ou plus couramment avec `a2enmod ssl`
    - et en créant un hôte virtuel permettant de traiter les requêtes relatives à ces transactions sécurisées :

             <IfModule mod_ssl.c>
                 Listen 443
                 <VirtualHost localhost:443>
                     SSLEngine On
                     SSLCertificateFile /etc/apache2/ssl/example.org.pem
                     SSLCertificateKeyFile /etc/apache2/ssl/example.org.key
                     …
                 </VirtualHost>
             </IfModule>

La directive `<IfModule>` permet d’éviter des plantages d’Apache lors d’un restart alors que le module ssl a été désactivé par mégarde.

# Nginx

1. Ajoutez `ssl` à la directive `listen` (méthode recommandée) ou ajoutez la directive `ssl on;` à votre configuration.
1. Ajoutez les chemins vers les fichiers du certificat et de la clé à l’aide des directives `ssl_certificate` et `ssl_certificate_key`;

        listen 443 ssl;
        ssl_certificate /etc/nginx/ssl/example.org.pem;
        ssl_certificate_key /etc/nginx/ssl/example.org.key;

**NB** Il existe un certain nombre de directives Apache et Nginx pour spécifier les protocoles utilisés (TLS 1, 1.1, 1.2), les *ciphers*, etc.
Je vous conseille **fortement** de lire leurs documentations et de suivre les recommendations de <https://bettercrypto.org> pour leur utilisation.

Pour vérifier la solidité de votre configuration SSL, vous pouvez tester votre site web sur <https://www.ssllabs.com/ssltest/> et sur <https://cryptcheck.fr/> (attention, ce dernier site note de façon **très** sévère).

# Gestion des certificats autosignés

Là encore, suivant la version d’Apache considérée et suivant les configurations déjà effectuées par la distribution utilisée (si Apache n’a pas été installé à partir de ses sources), plusieurs techniques permettent de gérer les certificats autosignés :

- Avec les versions 1.x d’Apache, un script `sign.sh` était fourni en standard.
  Ce script permettait alors de générer un fichier `.crt` à partir d’un fichier `.csr`^[ Ceci dit, les configurations d’Apache via cette technique posaient un certain nombre de problèmes. En particulier, Apache devait être exécuté de manière manuelle de façon à ce que la phrase clé puisse être saisie lors du démarrage. Des directives telles que `SSLPassPhraseDialog` — indiquant l’emplacement d’un programme devant retourner la phrase en question — pouvaient alors contourner le problème et permettre une exécution automatique d’Apache, ce qui est généralement le mode de lancement souhaité.] :

         ./sign.sh example.org.csr

- Avec les versions 2.x d’Apache, un script `apache2-ssl-certificate` permet de générer automatiquement un certificat autosigné sans même avoir à générer au préalable une clé de serveur.
  C’est donc une procédure simple permettant d’obtenir très rapidement le certificat souhaité.
  Ce script n’étant pas fourni dans Debian, on peut se tourner vers l’utilitaire `make-ssl-cert` du paquet `ssl-cert`.
- Sinon, il est toujours possible de signer son certificat à la main via l’utilitaire `openssl` :

         openssl x509 -in example.org.csr -out example.org.pem -req -signkey cleAC.key \
             -days 365

# Liens utiles

- <https://blog.eleven-labs.com/fr/comprendre-ssl-tls-partie-1/>
- <https://blog.eleven-labs.com/fr/comprendre-ssl-tls-partie-2-chiffrement/>
- <https://blog.eleven-labs.com/fr/comprendre-le-ssltls-partie-3-certificats/>
- Recommandations d’experts sur les *ciphers* et autres éléments de configuration à utiliser : <https://bettercrypto.org/>
- Tester la sécurité et les bonnes pratiques de son serveur :
  - <https://www.ssllabs.com/ssltest/>
  - <https://cryptcheck.fr> (Attention, celui-ci note **très** sévèrement)

# Un peu de pratique

**Attention** : ne pas inclure ces manipulations dans les réponses aux exercices.

**Apache et Nginx**

1. Mettez en place SSL en utilisant un certificat autosigné.
   Profitez-en pour étudier attentivement la documentation des composantes que vous aurez à manipuler.
1. Faites cohabiter sur votre serveur un hôte sans chiffrement ainsi qu’un hôte sécurisé.
1. Modifiez votre serveur sécurisé en utilisant cette fois-ci un certificat signé par une autorité de certification (la création d’une autorité de certification revient à générer un certificat autosigné, dont la clé privé sert à signer le certificat de votre serveur. N’oubliez pas que les moteurs de recherche sont vos amis.).
    - Que constatez-vous lors de la connexion à votre serveur ?
    - Installez le certificat de l’autorité de certification dans votre navigateur.
    - Que constatez-vous lors de la connexion à votre serveur^[ non, je ne suis pas gâteux, la question est répétée pour une bonne raison] ?
1.  Utilisez une règle de réécriture pour obliger l’utilisation du serveur sécurisé pour toute autre machine que celle hébergeant le serveur.

