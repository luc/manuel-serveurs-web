# Exercices

Les exercices sont à rendre sur <https://lstu.fr/asrall-web-tp2>.
Envoyez vos réponses dans un fichier appelé `tp2_votre_nom.txt`.

Vous pouvez envoyer un seul fichier pour un binôme, mais pensez bien à mettre vos deux noms dans le nom du fichier.

## Général

1. Définir des expressions régulières permettant la définition de directives pour les répertoires :
    - contenant la chaîne `foo` n’importe où ;
    - contenant une heure n’importe où ; formatée comme dans l’exemple `14h42` ;
    - finissant par la chaîne `html` ;
    - ne contenant pas la lettre « e ».

## Apache

1. Comment définir un conteneur relatif à toutes les images GIF, PNG et JPEG (ne pas oublier de considérer les extensions `.jpg` et `.jpeg`) ?
1. À partir d’un répertoire donné, installez un contrôle d’accès par hôtes en implantant les restrictions suivantes :
    - le sous-répertoire `infos` est en accès exclusif depuis votre machine ;
    - le sous-répertoire `strategie` est en accès autorisé uniquement à 2 machines particulières de votre salle ;
    - le sous-répertoire `docs` est en accès interdit à une machine particulière de la salle.

    Réalisez ces restrictions en utilisant des directives `<Directory>`, puis en vous basant sur l’utilisation des fichiers `.htaccess`.

## Nginx

1. À partir d’un répertoire donné, installez un contrôle d’accès par hôtes en implantant les restrictions suivantes :
    - le sous-répertoire `infos` est en accès exclusif depuis votre machine ;
    - le sous-répertoire `strategie` est en accès autorisé uniquement à 2 machines particulières de votre salle ;
    - le sous-répertoire `docs` est en accès interdit à une machine particulière de la salle.

