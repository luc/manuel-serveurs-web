# vim:set ft=make:
$(CACHE)_correction.pdf: TITLE = Reverse-proxy \& cache
$(CACHE).pdf: TITLE = Reverse-proxy \& cache
$(CACHE).pdf: QUOTE = What's wrong with that?
$(CACHE).pdf: QUOTEAUTHOR = Le dixième docteur
$(CACHE).pdf: FROM = Sébastien \textsc{Jean}
$(CACHE).html: TITLE = Reverse-proxy & cache
$(CACHE).html: QUOTE = What's wrong with that?
$(CACHE).html: QUOTEAUTHOR = Le dixième docteur
$(CACHE).html: FROM = Sébastien Jean
