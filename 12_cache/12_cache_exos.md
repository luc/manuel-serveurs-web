# Exercices

## Nginx

On considérera que Nginx écoute sur le port 81 et Apache sur le port 80.

Dans `/etc/nginx/nginx.conf`, ajoutez :

    proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=cache:10m inactive=7d max_size=700m;

Dans `/etc/nginx/sites-enabled/default` :

    location / {
        include proxy_params;
        proxy_pass http://127.0.0.1/;
        proxy_cache cache;
        proxy_cache_valid 12h;
        expires 12h;
        proxy_cache_use_stale error timeout invalid_header updating;
    }

Bien évidemment, si vous faites écouter apache sur un autre port que le 80, ajoutez le port à l’adresse de la directive `proxy_pass`.

Lancez les commandes suivantes :

    siege -c 300 --time=20s http://localhost/
    siege -c 300 --time=20s http://localhost:81/

Comparez les performances.

Supprimez la configuration ajoutée ayant trait au cache (tout ce qui commence par `proxy_cache` et `expires`) et redémarrez Nginx.

Recommencez. Que constatez-vous ?

Remettez `/etc/nginx/sites-enabled/default` dans son état de départ, sauf le port 81 :

    location / {
        try_files $uri $uri/ =404;
    }

Relancez Nginx.

Relancez le benchmark

Que constatez-vous, que ce soit pour les performances pures (hits, nombre de transactions par secondes, etc.) ou sur l’impact sur la charge de votre machine ?

## Apache

Définissez un hôte virtuel par IP, écoutant sur localhost sur le port 82 (ou autre si non disponible).

Choisissez un module de cache (disque ou mémoire) et activez-le^[ et on lit la doc sur <http://httpd.apache.org/docs/2.4/en/mod/mod_cache.html> siouplaît].

Refaites le benchmark de `http://localhost` (utilisez les valeurs de l’Apache non configuré comme cache ou comme proxy des exercices précédents pour comparer).
Que constatez-vous ?

