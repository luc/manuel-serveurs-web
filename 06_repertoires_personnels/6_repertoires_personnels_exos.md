# Exercices

## Apache

1.  Après avoir défini les répertoires utilisateurs dans le répertoire `.public_html` de leur `home` respectif, désactivez le répertoire personnel d’un des utilisateurs de votre système.
Effectuez ensuite un filtrage des fichiers PNG pour tous les utilisateurs sauf un.

