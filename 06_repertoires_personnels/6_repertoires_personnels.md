# Gestion des répertoires personnels des utilisateurs

**NB** : Nginx n'a pas d'équivalent pour cela, bien qu'on puisse bidouiller pour émuler cette fonctionnalité^[ voir le cours sur la configuration de base].

La directive `UserDir` permet d’autoriser les utilisateurs d’un système à publier leur propres pages Web.
Les pages Web d’un utilisateur `toto` (`toto` étant le login UNIX) seront alors accessibles *via* une URL du type <http://www.example.org/~toto>.

La directive `UserDir` peut prendre trois valeurs différentes pour créer l’association entre l’URL présentée ci-dessus et un répertoire physique du serveur :

- *Un répertoire relatif* : ce nom de répertoire correspond alors à un sous-répertoire présent dans le répertoire racine des utilisateurs.
Ce répertoire racine devient alors en quelque sorte le `DocumentRoot` des ressources Web de cet utilisateur ;

        UserDir .public_html

- *Un chemin absolu* : le nom de répertoire associé à la directive `UserDir` devient alors un répertoire racine contenant un sous-répertoire par utilisateur (dont les noms sont les logins des utilisateurs) ;

        UserDir /var/www/pagespersos

- *Un chemin absolu avec un emplacement* : ce répertoire se définit alors sensiblement de la même manière que pour le chemin absolu, mais en ajoutant le caractère « \* » qui sera remplacé par le login des différents utilisateurs.

        UserDir /global/persos/*/www

Il est également possible d’activer ou de désactiver ces répertoires personnels pour certains utilisateurs.
Ces modalités sont également définies grâce à la directive `UserDir`, qui peut alors prendre trois valeurs différentes :

- `UserDir disabled user1 user2 …` : permet de désactiver les répertoires personnels des utilisateurs passés en paramètre ;
- `UserDir disabled` : permet de désactiver tous les répertoires personnels, sauf ceux explicitement autorisés ;
- `UserDir enabled user1 user2 …` : permet d’activer explicitement certains répertoires personnels.

```
UserDir www
UserDir disabled
UserDir enabled toto
```

ou

```
UserDir www
UserDir disabled root webmaster
```

