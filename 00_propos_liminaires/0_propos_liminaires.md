- Choisissez un éditeur de texte (Vim, Nano, Emacs^[ bien qu’Emacs soit un excellent système d’exploitation auquel il ne manque qu’un bon éditeur de texte, certains masochistes s’obstinent encore à l’utiliser comme éditeur de texte]…), apprenez à le connaître (sauter à telle ligne, faire une recherche…) et à le customiser (affichage des numéros de ligne, coloration syntaxique…).
La modification des fichiers de configuration ne doit pas se faire avec une interface graphique mais en console : vous n’aurez pas d’environnement du bureau et d’applications graphiques sur les serveurs que vous gérerez.
- Il est préférable d’effectuer les TPs sur une machine virtuelle afin d’éviter les effets de bord avec les autres cours que vous suivez^[ `libvirt` avec `virt-manager` est très simple d’emploi].
*Cette machine virtuelle n’a pas besoin d’une interface graphique mais juste d’un serveur SSH.*
- Nous allons étudier Apache et Nginx. Vous pouvez, si vous le souhaitez, créer une VM dédiée à Apache et une dédiée à Nginx, histoire d’éviter de s’emméler les pinceaux.
- Installez `etckeeper` sur votre machine de travail ou transformez votre `/etc` en dépôt git : vous pourrez aisément voir la différence entre votre configuration pour les exercices et la configuration précédente.
- Installez `htop` et `multitail` sur votre machine de travail.
Le premier permet d’observer les processus de votre machine, le second est un `tail` amélioré, qui sera votre meilleur ami au moment de consulter des journaux systèmes (*logs*).
- J’aime beaucoup les notes de bas de pages, donc lisez-les, elles ne sont pas là pour décorer :p
- Les supports de cours sont disponibles sur <https://lstu.fr/asrall> et en version web sur <https://luc.frama.io/cours-asrall/serveurs_web/index.html>.
- Lisez et appliquez les conseils disponibles sur <https://luc.frama.io/cours-asrall/tips/index.html>.
