# Exercices

Les exercices sont à rendre sur <https://lstu.fr/asrall-web-tp3>.
Envoyez vos réponses dans un fichier appelé `tp3_votre_nom.txt`.

Vous pouvez envoyer un seul fichier pour un binôme, mais pensez bien à mettre vos deux noms dans le nom du fichier.

*Pour chacun des sites mis en place, définissez un document HTML minimal permettant de voir quel est le site correspondant à chaque requête.*
*Un simple `echo hostname > index.html` suffira.*

## Apache ET Nginx

Chaque question est donc à traiter deux fois.

1. Définissez 2 hôtes virtuels par adresse.
  Pour cela, associez plusieurs adresses IP différentes à votre interface réseau.
  Si vous utilisez une machine virtuelle, vous pouvez ajouter plusieurs adresses IPs à celle-ci, et tester les différentes adresses IP depuis l'intérieur de la machine virtuelle avec `curl` ou un navigateur texte comme `lynx` ou `w3m`.
1. Définissez 2 hôtes virtuels par nom.
  Utilisez la commande `telnet` pour mettre en évidence le mécanisme de sélection de l’hôte.
  Pour une utilisation directe dans votre navigateur, modifiez le fichier `/etc/hosts`.
1. Mettez en place un ensemble d’hôtes virtuels dynamiques : en particulier, définissez plusieurs sites tels qu’ils soient accessibles via une url du type `http://site.tld`

