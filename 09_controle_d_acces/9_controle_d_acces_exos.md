# Exercices

1. **Apache et Nginx** : Protégez un de vos répertoires au moyen d’une authentification en mode *basic*.
    - Définissez un fichier de mot de passe avec 3 utilisateurs différents.
    - Définissez le fichier `.htaccess` en n’autorisant que 2 des 3 utilisateurs déclarés (**Apache**).
      Vérifiez que le comportement des accès est bien conforme à ce que vous attendez.
    - **Apache** : En cherchant de la documentation sur la directive `AuthGroupFile`, définissez maintenant plusieurs groupes d’utilisateurs et fondez votre nouvelle stratégie d’autorisation uniquement sur les groupes.
1. **Apache** : Réalisez la question précédente en utilisant cette fois-ci le mode `digest` (si possible).
1. Pour les 2 précédentes questions, quelles précautions avez-vous prise pour la protection des fichiers de mots de passe, de groupe (emplacement, droits, autre…) ?
   Quelles précautions la configuration par défaut de votre distribution a-t’elle mise en place ?

