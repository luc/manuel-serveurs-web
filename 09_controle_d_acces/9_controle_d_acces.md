Nous avons déjà étudié précédemment quelles étaient les possibilités de restriction en fonction de l’origine d’un client (directives `Require` pour Apache et `allow` et `deny` pour Nginx).
D’autres directives permettent de contrôler l’accès à une page ou à un ensemble de pages.

Le protocole prévoit 2 méthodes d’authentification différentes : le mode *basic* (où les informations correspondantes transitent en clair) et le mode *digest* (où les informations correspondantes sont chiffrées).

**NB** : Nginx ne supporte pas le mode *digest*, uniquement le *basic*.

# L’authentification de base

Pour ce premier mode, il faut tout d’abord créer un fichier texte contenant les logins et les mots de passe.
Le logiciel `htpasswd` peut être utilisé pour cela.

Il accepte en particulier les options suivantes :

- `-c` : création d’un nouveau fichier,
- `-b` : considère le mot de passe de la ligne de commande au lieu de le demander interactivement.

Pour créer le fichier : `man htpasswd`^[ Bam ! RTFM !].

Une fois le fichier des mots de passe créé, il est alors nécessaire de l’activer au niveau du serveur Web.

## Apache

Cela se fait assez souvent^[ mais cela **n’a strictement rien d’obligatoire**] dans le fichier `.htaccess` du répertoire que l’on souhaite protéger.

    AuthName "Nom de mon domaine (ou n’importe quoi d’autre)"
    AuthType basic
    AuthBasicProvider file
    AuthUserFile /usr/local/apache2/auth/userfile
    Require user user1 user2 …

Dans l’exemple précédent, l’authentification en elle-même est assurée par les 4 premières lignes, commençant toutes par `Authxxx`.
La dernière ligne est utilisée une fois l’authentification effectuée.
C’est une directive d’*autorisation*, détaillant les utilisateurs autorisés^[ <http://httpd.apache.org/docs/2.4/mod/core.html#require>].

Les directives d’authentification (`Authxxx`) sont fournies par différents modules Apache (via des fichiers, une base de données, un annuaire LDAP…).
Regarder les modules dont le nom commence par `mod_auth` sur <https://httpd.apache.org/docs/2.4/mod/>.

- `AuthName` sert généralement à donner une indication à la personne voulant s’authentifier… ou pas.
Mettez ce que vous voulez.
- `AuthType` sert à indiquer le module d’authentification utilisé : `basic`, `digest`, `form`, etc.
- `AuthBasicProvider` est une directive propre au module `mod_auth_basic` et indique la source de données à utiliser pour l’authentification : fichier, base de données, annuaire LDAP, etc.
- `AuthUserFile` est une directive propre au module `mod_authn_file`, que nous avons désigné comme fournisseur de la source de données pour l’authentification avec `AuthBasicProvider`.

## Nginx

Le support de l’authentification est plus sommaire dans Nginx. Ainsi, il n’est pas possible de spécifier un sous-ensemble des utilisateurs autorisés :

    location / {
        auth_basic           "closed site";
        auth_basic_user_file conf/htpasswd;
    }

- `auth_basic` peut prendre une chaîne de caractères en argument ou `off` (pour désactiver l’authentification).
- `auth_basic_user_file` prendra en argument le chemin du fichier des utilisateurs et de leurs mots de passe.
Cet argument peut inclure des variables.

Le module fournissant l’authentification est `ngx_http_auth_basic_module`^[ <http://nginx.org/en/docs/http/ngx_http_auth_basic_module.html>].

# L’authentification digest

Elle assure plus de sécurité car, avec ce système, le mot de passe des utilisateurs ne transite pas en clair sur le réseau.
Si son principe de fonctionnement est différent de la méthode *basic*, son principe de mise en œuvre est relativement similaire.
La commande `htdigest` et les directives `AuthGroupFile` et `AuthDigestDomain` constituent de bons points d’entrée pour de la recherche de documentation.

En complément, la page <https://httpd.apache.org/docs/2.4/howto/auth.html> présente une synthèse des solutions d’authentification, d’autorisations et de contrôle d’accès.
**À lire**.

