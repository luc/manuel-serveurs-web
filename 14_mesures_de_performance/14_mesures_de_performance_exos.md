# Exercices

Ces exercices ne sont pour une fois pas à rendre :-)

1. Réalisez un petit shell^[ ou n'importe quel langage, je ne suis pas (trop) sectaire] script pour tester plusieurs URLs sur votre serveur (une page statique, un CGI, un FastCGI, peut-être du PHP, etc.) et affichez le tout à l’écran de manière ordonnée.
1. Installez Locust sur la machine virtuelle où est installée votre serveur web, créez un petit scénario de test et testez votre serveur.
Générez un graphique à partir des données en CSV (utilisez ce que vous voulez : LibreOffice, un script de votre cru, etc.)
1. Testez le serveur depuis la machine hôte (ie, pas la VM).
Y-a-t'il une différence de performance ? Qu'en déduisez-vous ?
1. Toujours avec Locust, faites des tests sur des pages de différentes tailles, des images, etc.
Faites varier les paramètres de threads de votre serveur, monitorez-le avec `htop`, bref, essayez de voir où se situe le goulot d'étranglement (ajoutez plus d'utilisateurs concurrents si vous n'atteignez pas les limites de votre serveur).
1. Mettez en place sur votre serveur plusieurs hôtes virtuels, dont un utilisant SSL.
   Comparez les performances entre eux.

Vous pouvez aussi analyser le fonctionnement côté serveur en étudiant les logs, et enfin terminer par une campagne de test recherchant la limite absolue de votre serveur.

# Bonus : Installation de Locust

Afin de ne pas pourrir votre machine avec les dépendances de locust, voici comment installer locust dans un `virtualenv` python.
Tout sera installé dans `~/.virtualenvs/locust`, que vous pourrez virer plus tard avec un simple `rm -rf`.

    apt-get install virtualenvwrapper
    apt-get install build-essential python-dev
    # Peut-être aussi python-openssl, je ne suis pas sûr
    source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
    mkvirtualenv locust
    pip install locustio

