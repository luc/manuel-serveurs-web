# Exercices

Les exercices sont à rendre sur <https://lstu.fr/asrall-web-tp5>.
Envoyez vos réponses dans un fichier appelé `tp5_votre_nom.txt`.

Vous pouvez envoyer un seul fichier pour un binôme, mais pensez bien à mettre vos deux noms dans le nom du fichier.

Les exercices ne sont à effectuer qu’avec Apache, sauf mention contraire.

1. Saisissez^[ Ou plutôt recopiez-les depuis le fichier .md disponible sur <https://lstu.fr/asrall>] les scripts CGI^[ pas les scripts FastCGI] shell, Perl et Ruby proposés dans le cours et exécutez-les de 2 manières différentes : en console et via le serveur Web.
   À vous de rechercher dans quels contextes les directives correspondantes peuvent être utilisées.
1. Par rapport à votre environnement, quelles sont les variables d’environnement définies spécifiquement par Apache ?
  Par quelles techniques peut-on personnaliser ces variables ?
1. `mod_actions` : À quel moment la directive `Action` est-elle utilisée ?
    Ainsi, si une action est définie sur un type HTML et qu’un script génère du HTML, cette directive intervient-elle ?
1. Installez `FastCGI` sur votre serveur, saisissez les scripts FastCGI et faites des tests de performances entre CGI et `FastCGI` sur 1000 requêtes par exemple (`siege` est votre ami).
1. Installez `spawn-fcgi` et faites fonctionner un des scripts fastcgi avec Nginx.
1. À partir de l’arborescence :

        td5/nimp
        td5/nimp/echo.cgi
        td5/nimp/echo.fcgi
        td5/nimp/args.sh
        td5/cgi-bin
        td5/cgi-bin/args.sh -> ../nimp/args.sh
        td5/cgi-bin/echo.cgi -> ../nimp/echo.cgi
        td5/fcgi-bin
        td5/fcgi-bin/echo.fcgi -> ../nimp/echo.fcgi
        td5/logs

    Réalisez la configuration d’un VirtualHost avec les propriétés
    suivantes :

    1. `td5/logs` devra contenir :
       - `access.log` (accès au virtualhost)
       - `error.log` (erreurs du virtualhost)
       - `cgi.log` (erreurs des scripts CGI et FastCGI)
    1. `http://localhost` liste le contenu du répertoire nimp
    1. `http://localhost/cgi-bin` et `http://localhost/fcgi-bin` interdits
    1. `http://localhost/cgi-bin/args.sh`, `http://localhost/cgi-bin/echo.cgi` et\
       `http://localhost/fcgi-bin/echo.fcgi` exécutent les scripts respectifs
    1. `http://localhost/args.sh`, `http://localhost/echo.cgi` et\
       `http://localhost/echo.fcgi` affichent le contenu des scripts dans le navigateur et n’en proposent pas le téléchargement.
    6.  le répertoire `/cgi-bin` ne devra pas exécuter de FastCGI (`.fcgi`) et réciproquement le répertoire `/fcgi-bin` ne devra pas exécuter de CGI (`.cgi`, `.sh`, `.pl` ou `.rb`)

    Pour réaliser cette configuration vous utiliserez les directives suivantes :
    - `Options`, `Indexes` et `SymLinksIfOwnerMatch`
    - `RemoveHandler`
    - `AddType`
    - `ScriptAlias`
    - `ScriptLog`
    - `ErrorLog`
    - `CustomLog`
    - `LogLevel`


